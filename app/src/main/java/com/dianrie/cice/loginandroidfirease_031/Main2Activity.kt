package com.dianrie.cice.loginandroidfirease_031

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.facebook.login.LoginManager
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main2.*

class Main2Activity : AppCompatActivity() {
    //private var auth: FirebaseAuth? = null
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        auth = FirebaseAuth.getInstance()
        //textViewSaludo.text = """Hola:${intent.getStringExtra("USUARIO")}"""
        textViewSaludo.text = """ID Usuario logeado: ${auth.currentUser?.uid}"""


    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.logOut -> {signOut()
                true}
            else -> super.onOptionsItemSelected(item)
        }
    }
    private fun signOut() {
        auth.signOut()
        LoginManager.getInstance().logOut()
        val intent = Intent(this, InicioActivity::class.java)
        startActivity(intent)
    }
}
