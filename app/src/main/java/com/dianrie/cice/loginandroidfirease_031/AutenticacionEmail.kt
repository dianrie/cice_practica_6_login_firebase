package com.dianrie.cice.loginandroidfirease_031

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.TextUtils
import android.util.Log
import android.view.View
import com.google.firebase.auth.*
import kotlinx.android.synthetic.main.activity_autenticacion_email.*
import android.util.Patterns


class AutenticacionEmail : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_autenticacion_email)
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

    }

    override fun onStart() {
        super.onStart()
        //Comprobamos si el usuario esta ya logeado
        if (auth.currentUser != null) {
            updateUI(auth.currentUser)
        }

    }

    /**
     * Send an email sign-in link to the specified email.
     */
    private fun sendSignInLink(email: String) {
        val settings = ActionCodeSettings.newBuilder()
            .setAndroidPackageName(
                packageName,
                false, null/* minimum app version */
            )/* install if not available? */
            .setHandleCodeInApp(true)
            .setUrl("https://google.es")
            .build()

        auth.sendSignInLinkToEmail(email, settings)
            .addOnCompleteListener { task ->

                if (task.isSuccessful) {
                    Log.d("TAG", "Link sent")
                    saveEmail(email)
                    Log.d("TAG", "email guardado: $email")
                } else {
                    val e = task.exception
                    Log.w("TAG", "Could not send link", e)

                    if (e is FirebaseAuthInvalidCredentialsException) {
                        fieldEmail.error = "Invalid email address."
                    }
                }
            }
    }

    /**
     * Funcion que se ejecuta al pulsar el boton ENVIAR LINK
     */
    fun botonLogin(view: View) {

        //validar datos introducidos en el campo fieldEmail
        val email = fieldEmail.text.toString()
        if (TextUtils.isEmpty(email) || !validarEmail(email)) {
            fieldEmail.error = "Please enter a valid Email."
            return
        }

        sendSignInLink(email)
    }

    /**
     * Crea un intent parar ir a otra activity
     */
    fun updateUI(user: FirebaseUser?) {
        val intent2 = Intent(this, Main2Activity::class.java)
        intent2.putExtra("USUARIO", "${user?.email}")
        startActivity(intent2)

    }

    /**
     * Guarda en Share Preferences una direccion de email pasada como String
     */
    //Guardar en Shared Preferences
    fun saveEmail(pendingEmail: String) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this).edit()
        //sharedPreferences.putString("nombreUusario1", "Paquito")
        sharedPreferences.putString("email", "$pendingEmail")
        sharedPreferences.apply()
    }


    /**
     * Comprueba si es una direccion de email y devuelve un Boolean
     */
    private fun validarEmail(email: String): Boolean {
        val pattern = Patterns.EMAIL_ADDRESS
        return pattern.matcher(email).matches()
    }
}

