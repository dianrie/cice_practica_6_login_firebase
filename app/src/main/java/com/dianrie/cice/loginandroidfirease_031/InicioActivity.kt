package com.dianrie.cice.loginandroidfirease_031

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthActionCodeException
import com.google.firebase.auth.FirebaseUser

class InicioActivity : AppCompatActivity() {
    private var emailLink: String = ""
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inicio)
        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        // Comprobamos si se ha pasado un intent con una direccion de correo
        if (intentHasEmailLink(intent)) {
            conectar()
        }

    }

    override fun onStart() {
        super.onStart()
        //Comprobamos si el usuario esta ya logeado
        if (auth.currentUser != null) {
            updateUI(auth.currentUser)
        }

    }
    /**
     * Crea un intent parar ir a otra activity
     */
    fun onLoginEmailPassword(view: View) {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
    /**
     * Crea un intent parar ir a otra activity
     */
    fun onLoginEmail(view: View) {
        val intent = Intent(this, AutenticacionEmail::class.java)
        startActivity(intent)
    }
    /**
     * Crea un intent parar ir a otra activity
     */
    fun onLoginFaccebook(view: View) {
        val intent = Intent(this, FacebookActivity::class.java)
        startActivity(intent)
    }

    /**
     * Crea un intent parar ir a otra activity
     */
    fun updateUI(user: FirebaseUser?) {
        val intent = Intent(this, Main2Activity::class.java)
        intent.putExtra("USUARIO", "${user?.email}")
        startActivity(intent)
    }

    /**
     * Comprueba si existe un email link en el intent devuelve un Boolean
     */
     fun intentHasEmailLink(intent: Intent?): Boolean {
        if (intent != null && intent.data != null) {
            val intentData = intent.data!!.toString()
            if (auth.isSignInWithEmailLink(intentData)) {
                return true
            }
        }

        return false
    }

    /**
     * Validamos el Email enviado con el recivido y si es el mismo realizamos la autenticacion con firebase
     */
     fun conectar() {
        emailLink = intent!!.data!!.toString()
        val pendingEmail =readEmail()
        Log.d("TAG", "PendingEmail:$pendingEmail")
        auth.signInWithEmailLink(pendingEmail, emailLink)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d("TAG", "signInWithEmailLink:success")
                    updateUI(task.result?.user)
                } else {
                    Log.w("TAG", "signInWithEmailLink:failure", task.exception)
                    updateUI(null)

                    if (task.exception is FirebaseAuthActionCodeException) {
                        Log.w("TAG", "signInWithEmailLink:failure", task.exception)
                        Toast.makeText(this, "${task.exception?.message}", Toast.LENGTH_LONG).show()
                    }
                }
            }

    }
    /**
     * Recupera la direccion de email de Share Preferences
     */
    fun readEmail(): String {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        return sharedPreferences.getString("email", "email no encontado")
    }
}
