package com.dianrie.cice.loginandroidfirease_031

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import android.util.Log
import android.util.Patterns
import com.google.firebase.auth.FirebaseUser


class MainActivity : AppCompatActivity() {

    //private var auth: FirebaseAuth? = null
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //auth = FirebaseAuth.getInstance()
        auth = FirebaseAuth.getInstance()

    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        if (currentUser != null) {
            updateUI(currentUser)
        }

    }

    fun registrarUsuario(view: View){
        //TODO: Validar contraseña
        //Si es correcta, registramos. comprobar contraseña mas de 6 y email
        //Si no es correcta, Toast.

        var email = emailEditText.text.toString()
        var password = paswordEditText.text.toString()

        if (!validarEmail(email)) {
            emailEditText.error = "Please enter a valid Email."
            return
        }
        if (TextUtils.isEmpty(password)) {
            paswordEditText.error = "Password must not be empty."
            return
        }

        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) {

                if (it.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("TAG", "createUserWithEmail:success")

                    val user = auth.getCurrentUser()
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("TAG", "createUserWithEmail:failure", it.exception)
                    Toast.makeText(this, "${it.exception?.message}", Toast.LENGTH_LONG).show()
                    //updateUI()
                }

                // ...
        }
    }
    fun loginUsuario(view: View){
        //TODO: Validar contraseña
        //Si es correcta, registramos. comprobar contraseña mas de 6 y email
        //Si no es correcta, Toast.


        var email = emailEditText.text.toString()
        var password = paswordEditText.text.toString()

        if (!validarEmail(email)) {
            emailEditText.error = "Please enter a valid Email."
            return
        }
        if (TextUtils.isEmpty(password)) {
            paswordEditText.error = "Password must not be empty."
           return
        }
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) {

                if (it.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("TAG", "createUserWithEmail:success")

                    val user = auth.getCurrentUser()
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("TAG", "createUserWithEmail:failure", it.exception)
                    Toast.makeText(this, "${it.exception?.message}", Toast.LENGTH_LONG).show()
                    //updateUI(null)
                }

                // ...
            }
    }

    fun updateUI(user: FirebaseUser?){
        //Todo: Comprobar si tenemos un usuario
        val intent = Intent(this, Main2Activity::class.java)
        intent.putExtra("USUARIO","${user?.email}")
        startActivity(intent)

    }

    private fun validarEmail(email: String): Boolean {
        val pattern = Patterns.EMAIL_ADDRESS
        return pattern.matcher(email).matches()
    }
}
